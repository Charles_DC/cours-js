/* getRandomInt: solution < max */
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
/* 2 <= solution <= 11 */
function drawBlackjack(){
    return 2 + getRandomInt(10);
}

let messageEl = document.querySelector("#message-el")
let affjeuxEl = document.querySelector("#affjeux-el")
let btnpartieEl = document.querySelector("#nvpartie-el");
let cartesEl = document.querySelector("#cartes-el");
// let sumEl = document.getElementById("sum-el")
let sumEl = document.querySelector("#sum-el");
let btnstopEl = document.querySelector("#stop-el");

let firstCard;
let secondCard;
let sum = 0;
let hasBlackjack = false
let isAlive = true
let message = ""

function resetGame(){
    sum = 0;
    hasBlackjack = false;
    isAlive = true;
    startGame();
}

function startGame(){
    btnpartieEl.innerHTML = "<button onclick='pioche()'>Piocher</button>";
    btnstopEl.innerHTML = "<button onclick='stop()'>Stop</button>";
    affjeuxEl.innerHTML  = "<span id='cartes-el'>Cartes : </span><br><span id='sum-el'>Somme : </span>"
    
    cartesEl = document.querySelector("#cartes-el");
    btnpartieEl = document.querySelector("#nvpartie-el");
    btnstopEl = document.querySelector("#stop-el");
    sumEl = document.querySelector("#sum-el");
    
    firstCard = drawBlackjack();
    secondCard = drawBlackjack();
    setSum(firstCard);
    setSum(secondCard);
    addCard(firstCard);
    addCard(secondCard);

    if (sum <= 20) {
        //new card
        message = "Veux-tu encore pioché ?"
    } else if (sum === 21){
        //Blackjack ! Youpi !
        message = "Vous avez un Blackjack!"
        hasBlackjack = true
    } else {
        //Lose :c
        message ="Dommage :c !"
        isAlive = false
    }
    setMessage(message);
}

function pioche(){
    firstCard = drawBlackjack();
    addCard(firstCard);
    setSum(firstCard);

    if (sum <= 20) {
        //new card
        message = "Veux-tu encore pioché ?"
    } else if (sum === 21){
        //Blackjack ! Youpi !
        message = "Vous avez un Blackjack!"
        hasBlackjack = true
    } else {
        //Lose :c
        message ="Dommage :c !"
        isAlive = false
    }

    setMessage(message);
}

function setMessage(message){
    messageEl.textContent = message;

}

function stop(){
    messageEl.textContent = "Partie arreté";
    btnpartieEl.innerHTML = "<button onclick='resetGame()'>Nouvelle partie</button>";
    btnstopEl.innerHTML = "";
}

function setSum(card){
    sum += card;
    sumEl.textContent = "Somme : " + sum
}

function addCard(card){
    cartesEl.textContent += card + ", "
}
