# JS cours complet

## Table des matières
[[_TOC_]]

## But
> Rendre acessible le dévelopement web lors d'une nuit de l'info
>
<br> Public cible :
1. Les reblochons
1. les adeptes de l'union des grands abrutis
1. Toutes les personnes issue de l'équipe des reblochons

Ce cours va suivre les étapes du cours de [▶️ JavaScript Programming - Full Course](https://www.youtube.com/watch?v=jS4aFq5-91M&t=7s)
<br>De plus, pour approfondir vos connaissances après ce cours il est recommandé d'aller voir le site [freeCodeCamp.org](https://www.freecodecamp.org)

---
## Chapitre 1
vous avez dans le dossier Chapitre_1/ tout le nécessaire pour le completer. Vous pouvez visiter la page index.html et index.css pour voir comment le code marche, surtout pour le ' onclick="" ' du HTML.
> Les zones à modifier sont des lignes vides précédé d'un commentaire.
* Pour ce chapitre vous trouverez un fichier solution.js, si vous avez un doute ou que vous bloquez sur un exercice, il peut vous aider.
### Premier exercice
Dans ce premier exercice, vous devez reproduire le bouton d'incrementation et afficher sa valeur.
```
// 1
// Faire un bouton qui incremente 
// Initialise le conteur à 0
// Écouter les clicks avec le 'onclick=""' build-in
// Incremente le conteur sur chaque click
// Change la valeur affiché sur la page web 
```

### Deuxième exercice
Comment faire un bouton de sauvegarde qui réinitialise l'incrémentation et affiche un message dans la console.
```
// 2
// Faire un bouton de sauvegarde
// Envoie un message dans la console avec la valeur du conteur
// Réhinitialise le conteur
// Change l'affichage
```
### Troisième exercice
Comment afficher un message de bienvenu sur la page, point bonus si on ajoute un emoji "👋" à chaque click.
```
// 3 
// charger/changer un paragraph dans l'HTML
// Que fait la fonction disparait() ?
// Comment appeler la fonction emoji plutot que disparait() ?
```
### Défis du chapitre
* Reproduire l'image defis_chapitre.png 
### Récap
1. Script tag
1. variables
1. numbers
1. strings
1. console.log()
1. function
1. The DOM
1. .getElementById()
1. .innerText
1. .textContent
---
## Chapitre 2
Un petit saut dans le css, totalement dispensable, [CSS-only acrylic Material](https://stackoverflow.com/questions/44522299/css-only-acrylic-material-from-fluent-design-system) ou simplement :
```
acrylic {
    
    backdrop-filter: blur(15px);
    background-color: rgba($color: #000000, $alpha: 0.5);


}
```
Code écrit par M.Pierson.
<br>
<b>avec une ombre pour plus d'effet !</b>
```
.shadow {
    border-radius: 1px;
    box-shadow: 0 10px 30px rgba(0, 0, 0, 0.1), 0 1px 8px rgba(0, 0, 0, 0.2);
} 
```
Sur cette état du css, commençons notre chapitre.
<br>
Comment pour le chapitre précédent il exite un fichier solutions.js, mais il existe aussi un fichier solution.html.
### Une Calculatrice
> Vous devez faire une calculatrice, pour cette exercice les deux nombres ne sont pas des entrées utilisateurs.
* Prennez 8 et 2 

À vous de récupérer les éléments de l'HTML, de mettre les fonctions et ne pas oublier de modifier l'HTML
```
indice:
    <button onclick="add()">ADD</button>
```
---
## Chapitre 3

Ces deux lignes sont égales :
```
let sumEl = document.getElementById("sum-el");
let sumEl = document.querySelector("#sum-el");
```
<I> Mais on préférera la deuxième car elle est plus précise. :) </I>

### Un BlackJack !
Faites un blackJack.<br>
Tous les fichiers nécessaire sont dans le dossier Chapitre_3/

---
## Chapitre 4