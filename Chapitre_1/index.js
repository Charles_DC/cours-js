// Indice: (document.getElementById("count-el").innerText = count) <=> (countEl.innerText = count)
//
// 1 Faire un bouton qui incremente 
// Initialise le conteur à 0
// Écouter les clicks avec le 'onclick=""' build-in
// Incremente le conteur sur chaque click
// Change la valeur affiché sur la page web 
//
// Récupère l'élément par l'id
let countEl = document.getElementById("count-el")
// Initialise la variable count à 0
let count = 0;
// definition de la fonction increment
function increment() {
    //Incrémente la variable count

    // Met à l'interieur de l'élément countEl la nouvelle valeur de count en textuel
    countEl.innerText = count;
}
//
// 2 Faire un bouton de sauvegarde
// Crée le bouton
// fait un rapport à la console du nombre et réhinitialise le conteur
function save(){
    console.log(count);
    //Réinitialise la valeur de count

    // Met à l'interieur de l'élément countEl la valeur de count

}
// Utilisation des strings de javascripts
let username = "peter";
let numberStr = "four";
let message = "You have " + numberStr +  " new notifications";
let messageToUser = message + ", " + username + "!";
console.log(messageToUser);
//
// 3 charger/changer un paragraph dans l'HTML
let name1 = "chouquette";
let greetings = "Bonjour, ";
let welcomEl = document.getElementById("welcome-el");
welcomEl.innerText = greetings + name1;
//
// Que fait cette fonction ?
function disparait(){
    welcomEl.innerText ="";
}
//Comment appeler cette fonction plutot que disparait() ?
function emoji(){
    // À quoi sert cette ligne ?
    welcomEl.innerText += " " + "👋";
}
// 42
// Défis du chapite :
// rendre un site tel que l'image defi_chapitre.png










