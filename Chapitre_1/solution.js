// document.getElementById("count-el").innerText = 5

// 1 Faire un bouton qui incremente 
let countEl = document.getElementById("count-el")
let count = 0;
function increment() {
    count++;
    countEl.innerText = count;
}

// 3 charger/changer un paragraph dans l'HTML
let name1 = "chouquette";
let greetings = "Bonjour, ";
let welcomEl = document.getElementById("welcome-el");
welcomEl.innerText = greetings + name1;

function emoji(){
    welcomEl.innerText += " " + "👋";
}

// Petit trik pour éviter de changer disparait dans l'exercice
function disparait(){
    welcomEl.innerText += " " + "👋";
}

// Défis du chapite :
// rendre un site tel que l'image defi_chapitre.png
let anciensEl = document.getElementById("anciens-el");
// 2 La fonction du bouton sauvegarde
function save(){
    anciensEl.textContent += " " + count + ","
    count = 0;
    countEl.innerHTML = count;
}

